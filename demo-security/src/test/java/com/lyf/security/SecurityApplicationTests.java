package com.lyf.security;

import com.lyf.data.structure.jpa.entity.crm.RolePermissionEntity;
import com.lyf.security.dao.crm.RolePermissionDao;
import com.lyf.security.dao.crm.UserDao;
import com.lyf.security.login.service.LoginService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.List;

@SpringBootTest
class SecurityApplicationTests {
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    UserDao userDao;
    @Autowired
    RolePermissionDao rolePermissionDao;
    @Autowired
    LoginService loginService;

    @Test
    void contextLoads() {
        //redisTemplate.opsForValue().set("user:test","token...");
//        userDao.listUsers("Admin1","123456").stream()
//        .forEach(u->{
//            System.out.println(u);});

//        userDao.findByUsernameAndPassword("Admin1","123456").stream()
//                .forEach(u->{
//                    System.out.println(u);});
    }
    @Test
    void testToken(){
        String token = DigestUtils.md5DigestAsHex("username".getBytes(StandardCharsets.UTF_8));
        System.out.println(token);
    }
    @Test
    void testRedisList(){//权限列表
//        redisTemplate.opsForList().leftPush("list:1","No1");//左侧插入？
//        redisTemplate.opsForList().leftPush("list:1","No2");

        List<String> list = redisTemplate.opsForList().range("list:1",0, redisTemplate.opsForList().size("list:1"));
        list.forEach(p->{
            System.out.println(p);
        });

    }


    @Test
    void mockData(){
       for(int i=2;i<8;i++){
           if(i==7||i==6)
               continue;
           RolePermissionEntity rolePermissionEntity = new RolePermissionEntity();
           rolePermissionEntity.setRoleId(2);
           rolePermissionEntity.setPermission(i);
           rolePermissionDao.save(rolePermissionEntity);
       }
    }

    @Test
    void authTest(){
        //loginService.saveToken("token1id","CommonAdmin1");
       // redisTemplate.opsForList().leftPush("permissionList:user1","hello1");
        for(int i=0;i<10;i++){
            String token = DigestUtils.md5DigestAsHex("hello".getBytes(StandardCharsets.UTF_8));
            System.out.println(token);
            System.out.println(DigestUtils.md5DigestAsHex(
                    "SystemAdmin1".getBytes(StandardCharsets.UTF_8)
            ));
        }

        // 5d41402abc4b2a76b9719d911017c592

        // f32fb46a967dc4573c463684eac50d5a

    }

}
