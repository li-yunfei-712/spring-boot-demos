package com.lyf.security.simple;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/21
 * @VERSION 1.0
 * @DESC
 */
@RestController
public class TestController {
    @RequestMapping("/hello")
    String hello(){
        return "hello";
    }
}
