package com.lyf.security.login.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/21
 * @VERSION 1.0
 * @DESC
 * 拦截器配置
 */
@Configuration
public class WebConfigurer implements WebMvcConfigurer {
    @Autowired
    LoginInterceptor loginInterceptor;
    @Autowired
    AuthenticInterceptor authenticInterceptor;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        System.out.println("拦截器配置");
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/userLogin");
        registry.addInterceptor(authenticInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/userLogin");
    }
}
