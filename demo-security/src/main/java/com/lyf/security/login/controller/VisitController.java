package com.lyf.security.login.controller;

import com.lyf.security.login.annotaion.RequiredPermission;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/21
 * @VERSION 1.0
 * @DESC
 */
@RestController
@RequestMapping("/visit")
public class VisitController {
    @RequiredPermission(value = "VisitResource1")
    @RequestMapping("/resource1")
    String resource1(){
        return "resource1";
    }
    @RequiredPermission(value = "VisitResource2")
    @RequestMapping("/resource2")
    String resource2(){
        return "resource2";
    }
}
