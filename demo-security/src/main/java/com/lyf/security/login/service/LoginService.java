package com.lyf.security.login.service;

import com.lyf.data.structure.jpa.entity.crm.CusPermissionEntity;

import java.util.List;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/21
 * @VERSION 1.0
 * @DESC
 */
public interface LoginService {
    // 验证用户名和密码正确性
    boolean validate(String username,String password);
    // 保存token
    boolean saveToken(String token,String username);//Integer accountId);
    // 查询权限列表 (Mark)
    List<String> listPermissions(String username);
}
