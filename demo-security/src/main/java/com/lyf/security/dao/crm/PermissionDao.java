package com.lyf.security.dao.crm;

import com.lyf.data.structure.jpa.entity.crm.CusPermissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/21
 * @VERSION 1.0
 * @DESC
 */
public interface PermissionDao extends JpaRepository<CusPermissionEntity,Integer> {
    @Override
    List<CusPermissionEntity> findAll();
}
