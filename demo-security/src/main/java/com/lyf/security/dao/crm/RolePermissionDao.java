package com.lyf.security.dao.crm;

import com.lyf.data.structure.jpa.entity.crm.RolePermissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/21
 * @VERSION 1.0
 * @DESC
 */
public interface RolePermissionDao extends JpaRepository<RolePermissionEntity,Integer> {
    @Override
    <S extends RolePermissionEntity> S save(S s);
    List<RolePermissionEntity> findByRoleId(Integer roleId);
}
