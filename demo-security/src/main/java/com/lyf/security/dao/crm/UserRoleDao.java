package com.lyf.security.dao.crm;

import com.lyf.data.structure.jpa.entity.crm.UserRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/21
 * @VERSION 1.0
 * @DESC
 */
public interface UserRoleDao extends JpaRepository<UserRoleEntity,Integer> {
    List<UserRoleEntity> findByUserId(Integer userId);
}
