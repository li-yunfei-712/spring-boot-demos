package com.lyf.security.dao.crm;

import com.lyf.data.structure.jpa.entity.crm.CusUserEntity;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/21
 * @VERSION 1.0
 * @DESC
 */
public interface UserDao extends JpaRepository<CusUserEntity,Integer>, JpaSpecificationExecutor {
    @Override
    <S extends CusUserEntity> boolean exists(Example<S> example);
    //List<CusUserEntity> findAllByUsernameAndPassword();
    //采用注解 (java 8才能省略 @Param())
    @Query(value = "SELECT * FROM cus_user " +
            "WHERE username = :username AND password= :password",nativeQuery = true)
    List<CusUserEntity> listUsers(@Param("username") String username, @Param("password") String password);
    // 最好不要直接通过并列查询 来检验《《《SQL注入问题？？？
    List<CusUserEntity> findByUsernameAndPassword(String username,String password);
    List<CusUserEntity> findByUsername(String username);
}
