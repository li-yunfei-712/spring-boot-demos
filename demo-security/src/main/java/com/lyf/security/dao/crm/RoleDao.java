package com.lyf.security.dao.crm;

import com.lyf.data.structure.jpa.entity.crm.CusRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/21
 * @VERSION 1.0
 * @DESC
 */
public interface RoleDao extends JpaRepository<CusRoleEntity,Integer> {
}
