package com.lyf.security.config.datasource.sources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/21
 * @VERSION 1.0
 * @DESC
 */
@EnableTransactionManagement
//@Component
@Configuration // Config注解和Component...
@EnableJpaRepositories(entityManagerFactoryRef = "crmEntityManagerFactory",
        transactionManagerRef = "crmTransactionManager",
        basePackages = {"com.lyf.security.dao.crm"})//配置Jpa信息
public class CrmDataSourceConfig {
    @Autowired
    @Qualifier("crmDataSource")
    DataSource crmDataSource;

    // 配置实体管理的地方
    @Bean(name = "crmEntityManagerFactory")
    LocalContainerEntityManagerFactoryBean crmEntityManagerFactory(EntityManagerFactoryBuilder builder){
        return builder.dataSource(crmDataSource)
                .packages("com.lyf.data.structure.jpa.entity.crm")
                .persistenceUnit("crmPersistenceUnit")
                .build();
    }

    // 配置事务管理
    @Bean
    TransactionManager crmTransactionManager(EntityManagerFactoryBuilder builder){
//       return new JpaTransactionManager(capEntityManagerFactory(builder).getObject());
        //    Method annotated with @Bean is called directly. Use dependency injection instead.
        return new JpaTransactionManager(crmEntityManagerFactory(builder).getObject());
    }


}

