package com.lyf.schedule.config;

import com.lyf.schedule.driver.CronExpressionDriver;
import com.lyf.schedule.task.Task1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/20
 * @VERSION 1.0
 * @DESC
 * 一、配置定时任务
 * 1. 配置任务、Cron表达式
 * 2. 动态设定Cron和Task（根据DB）
 *
 * 二、难点
 * 1. 主要掌握理解cron 表达式
 *
 */
@EnableScheduling
@Configuration
public class ScheduleConfig implements SchedulingConfigurer {
    @Autowired
    CronExpressionDriver cronExpressionDriver;
    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
           scheduledTaskRegistrar.addCronTask(new Task1(),cronExpressionDriver.getCron());
        //scheduledTaskRegistrar.addCronTask();
    }
}
