package com.lyf.schedule.driver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/20
 * @VERSION 1.0
 * @DESC
 * 1. 模拟Cron表达式；
 *
 * 2. 接DB
 */
@Configuration
public class CronExpressionDriver {
    @Bean
    public String getCron(){
        return "0/3 * * * * ? ";
    }
}
