//package com.lyf.schedule.demo;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
///**
// * @AUTHOR LYF
// * @DATE 2022/1/20
// * @VERSION 1.0
// * @DESC
// * 关于定时任务：
// * https://www.cnblogs.com/jing99/p/11546559.html
// *
// */
//@EnableScheduling
//@Component
//public class PracticeSchedule {
//    @Bean
//    @Scheduled(cron = "* * * * * ?")
//    void run(){
//        while (true){
//            System.out.println("task...");
//            try{
//                Thread.sleep(10000);
//            }catch (Exception e)
//            {
//                e.printStackTrace();
//            }
//
//        }
//    }
//}
