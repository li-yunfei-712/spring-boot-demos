package com.lyf.schedule.task;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/20
 * @VERSION 1.0
 * @DESC
 */
public class Task1 implements Runnable{

    @Override
    public void run() {
        System.out.println(System.currentTimeMillis()+"I am a test task!!!");
    }
}
