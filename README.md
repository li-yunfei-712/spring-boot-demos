# spring-boot-demos

#### 介绍
springboot常用技术栈学习示例,基本框架初次从创建。主要的技术栈含以下：
1. 数据版块
(1)JPA , myabtis 
(2)JDBC
(3)MongoDb
(4)redis

2. 消息件
(1)Mq
(2)kafka

3. Web交互
（1）拦截器，事务，AOP（数据版块）
（2）SpringMVC

4. 日志系统
（1）Log4j2

5. 网络部分
（1）

#### 软件架构

![web应用整体结构；以此为基准进行相关的主流技术栈（如JPA，Mybatis，Mq，Kafka等）学习](https://images.gitee.com/uploads/images/2022/0119/145654_69e1b56a_8844212.png "Snipaste_2022-01-19_14-56-34.png")


#### 安装教程

1. MySQL: 8.0.21
2. docker 
3. mongo、Redis
[安装推荐](http://https://www.runoob.com/)

