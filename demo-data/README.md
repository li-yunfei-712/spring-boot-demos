# 工程简介
一、持久层基本知识

1. 基础概念

(1)关于POJO和EJO 
POJO:简单Java对象;EJO:WEB开发
(2)关于DO,DTO,VO,ENTITY,JavaBean,DAO,Repository
(3)大致工程分层:
web
service:
manager:通用
dao(数据访问层):
数据库/DB/数据源连接等
该层主要是对dao层的研究:

2. 基本研究点(整体)

(1) 数据库的研究（关系类:Mysql;非关系类:MongoDB（文档性）,redis(做缓存)）

(2) 数据库的基本使用:用户连接,用户管理，基本SQL的使用

(3) 数据的访问:JDBC进行连接,数据源的配置(采用CRUID或者HArika),数据库缓存(自带的缓存),
数据库的事务处理

(4) 基本框架的使用:repository,进行SQL的基本使用，及其简单封装！！
为service提供服务;
理解掌握:ORM;对比hibernate与mybatis
hibernate:JPA
mybatis,mybatis-plus

(5) 在Java中 表对应的实体理解 DO,Bean/Entity,DTO,BO,VO




3. 拓展技术点

(1)多数据源的配置

(2)复杂查询:分页的使用

(3)


二、一些思考

1. 关于连接URL的问题    类型:username:pwd@host:port/db

三、问题记录

1. mongodb授权问题:每个数据库独立一个用户，需要单独授权
关于创建用户问题:https://www.jianshu.com/p/91f98a5e9b28
   

2. 关于 persistence unit的作用？



# 延伸阅读

