# 基本结构
# SELECT [col,] FROM
# table
# WHERE ...
# GROUP BY
# HAVING
# ORDER BY
# 同时 SELECT 可以嵌套，另外注意函数的使用

# 简单条件查询
# SELECT A.aid,A.aname,A.percent
# FROM agents A
# WHERE city = '南京'

SELECT count(city='南京') AS nanjing_city_cnt
FROM agents
