package com.lyf.data.db.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC redis自带模板使用
 * Redis使用，需要进行序列化，否则直接使用redistemplate会出现乱码
 */
@Component
public class RedisTemplateDemo {
    @Autowired
    RedisTemplate redisTemplate;
    //@Bean
    public void SimpleBaseTest(){
        Object o = redisTemplate.opsForValue().get("hello");
        System.out.println(o);
        redisTemplate.opsForValue().set("test:1","hello!!");
        redisTemplate.opsForList().leftPush("test:list","hello-01");
        System.out.println("时效时长:"+redisTemplate.getExpire("test:1").intValue());

    }
}
