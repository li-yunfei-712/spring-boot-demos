package com.lyf.data.db.mongo;

import com.lyf.data.domain.common.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC mongo自带模板使用
 */
@Component
public class MongoTemplateDemo {
    @Autowired
    MongoTemplate mongoTemplate;
    public void testBaseUse(){
        Log log = new Log();
        log.setDate(new Date());
        log.setEvent("Test.Exception...");
        log.setLevel("WARNING");
        //mongoTemplate.
        //mongoTemplate.insert(log,"log");
        //mongoTemplate.insert(log,"log");
        // 文档型,只管添加即可，但是会影响可维护型？，最好是文档归类
        //mongoTemplate.insert(new User("lyf",21),"log");// 测试不同类是否会冲突

        //
        // mongoTemplate.remove(User.class);// new User("lyf",21)
        // mongoTemplate.re
        // mongoTemplate.remove(new User("61e7ad6d5da51806b95479f1","lyf",21),"log");


    }
}
