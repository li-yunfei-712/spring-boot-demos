//package com.lyf.data.config.datasource.sources;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.stereotype.Component;
//import org.springframework.transaction.TransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import javax.sql.DataSource;
//
///**
// * @AUTHOR LYF
// * @DATE 2022/1/19
// * @VERSION 1.0
// * @DESC
// */
//@EnableTransactionManagement
////@Component
//@Configuration // Config注解和Component...
//@EnableJpaRepositories(entityManagerFactoryRef = "slave1EntityManagerFactory",
//        transactionManagerRef = "slave1TransactionManager",
//        basePackages = {"com.lyf.data.structure.jpa.repository.slave1"})//配置Jpa信息
//public class Slave1DataSourceConfig {
//    @Autowired
//    @Qualifier("slave1DataSource")
//    DataSource slave1DataSource;
//
//    // 配置实体管理的地方
//    @Bean(name = "slave1EntityManagerFactory")
//    LocalContainerEntityManagerFactoryBean slave1EntityManagerFactory(EntityManagerFactoryBuilder builder){
//        return builder.dataSource(slave1DataSource)
//                .packages("com.lyf.data.structure.jpa.entity.slave1")
//                .persistenceUnit("slave1PersistenceUnit")
//                .build();
//    }
//
//    // 配置事务管理
//    @Bean
//    TransactionManager slave1TransactionManager(EntityManagerFactoryBuilder builder){
////       return new JpaTransactionManager(capEntityManagerFactory(builder).getObject());
//        //    Method annotated with @Bean is called directly. Use dependency injection instead.
//        return new JpaTransactionManager(slave1EntityManagerFactory(builder).getObject());
//    }
//
//
//}
