package com.lyf.data.config.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC 配置数据源,可进行数据源类型的转换
 * 配置DataSource
 */
@Component
public class DataSourceConfig {
    @Primary
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.cap")
    DataSource capDataSource(){
       return DataSourceBuilder.create().type(DruidDataSource.class).build();
    }
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.slave1")
    DataSource slave1DataSource(){
        return DataSourceBuilder.create().type(DruidDataSource.class).build();
    }
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.crm")
    DataSource crmDataSource(){
        return DataSourceBuilder.create().type(DruidDataSource.class).build();
    }
}
