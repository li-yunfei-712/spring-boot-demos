package com.lyf.data.config.datasource.mybatis;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/23
 * @VERSION 1.0
 * @DESC
 */
@Configuration
@MapperScan(basePackages = "com.lyf.data.structure.mybatis.mapper.crm",
        sqlSessionFactoryRef = "crmSqlSessionFactory")
public class CrmDataSourceConfig {
    @Bean
    public SqlSessionFactory crmSqlSessionFactory(@Qualifier("crmDataSource") DataSource datasource)
            throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(datasource);
//        bean.setMapperLocations(
//                // 设置mybatis的xml所在位置
//                new PathMatchingResourcePatternResolver().getResources("classpath*:mapping/test01/*.xml"));
//
        return bean.getObject();
    }
    @Bean("crmSqlSessionTemplate")
    public SqlSessionTemplate crmSqlSessionTemplate(
            @Qualifier("crmSqlSessionFactory") SqlSessionFactory sessionFactory) {
        return new SqlSessionTemplate(sessionFactory);
    }


}
