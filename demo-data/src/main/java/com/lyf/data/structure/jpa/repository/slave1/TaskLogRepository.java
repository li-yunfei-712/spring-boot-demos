package com.lyf.data.structure.jpa.repository.slave1;

import com.lyf.data.structure.jpa.entity.slave1.TaskLog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC
 */
public interface TaskLogRepository extends JpaRepository<TaskLog,Integer> {
    @Override
    <S extends TaskLog> S save(S s);
}
