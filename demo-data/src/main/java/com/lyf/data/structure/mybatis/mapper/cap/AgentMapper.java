package com.lyf.data.structure.mybatis.mapper.cap;

import com.lyf.data.structure.jpa.entity.cap.AgentsEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/23
 * @VERSION 1.0
 * @DESC
 */
//@Repository
@Mapper
public interface AgentMapper {
    @Select("SELECT * FROM agents")
    @Transactional(isolation = Isolation.DEFAULT)
    List<AgentsEntity> listAgents();
}
