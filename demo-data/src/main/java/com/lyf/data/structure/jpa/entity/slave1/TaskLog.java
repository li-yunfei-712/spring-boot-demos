package com.lyf.data.structure.jpa.entity.slave1;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC
 */
@Entity
@Table(name = "task_log", schema = "slave_1")
public class TaskLog {
    private int id;
    private Timestamp date;
    private String level;
    private String content;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "date")
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Basic
    @Column(name = "level")
    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskLog taskLog = (TaskLog) o;

        if (id != taskLog.id) return false;
        if (date != null ? !date.equals(taskLog.date) : taskLog.date != null) return false;
        if (level != null ? !level.equals(taskLog.level) : taskLog.level != null) return false;
        if (content != null ? !content.equals(taskLog.content) : taskLog.content != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (level != null ? level.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }
}
