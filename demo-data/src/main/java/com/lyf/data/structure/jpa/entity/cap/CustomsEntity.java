package com.lyf.data.structure.jpa.entity.cap;

import javax.persistence.*;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC
 */
@Entity
@Table(name = "customs", schema = "app", catalog = "")
public class CustomsEntity {
    private String cid;
    private String cname;
    private String city;
    private Double disCnt;

    @Id
    @Column(name = "cid")
    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    @Basic
    @Column(name = "cname")
    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    @Basic
    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "dis_cnt")
    public Double getDisCnt() {
        return disCnt;
    }

    public void setDisCnt(Double disCnt) {
        this.disCnt = disCnt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomsEntity that = (CustomsEntity) o;

        if (cid != null ? !cid.equals(that.cid) : that.cid != null) return false;
        if (cname != null ? !cname.equals(that.cname) : that.cname != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (disCnt != null ? !disCnt.equals(that.disCnt) : that.disCnt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cid != null ? cid.hashCode() : 0;
        result = 31 * result + (cname != null ? cname.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (disCnt != null ? disCnt.hashCode() : 0);
        return result;
    }
}
