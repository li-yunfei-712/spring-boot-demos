package com.lyf.data.structure.jpa.repository.slave1;

import com.lyf.data.structure.jpa.entity.slave1.TaskLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC
 */
public interface TaskLogEntityRepository extends JpaRepository<TaskLogEntity,Integer> {
    @Override
    <S extends TaskLogEntity> S save(S s);
    @Override
    List<TaskLogEntity> findAll();
    @Override
    <S extends TaskLogEntity> S saveAndFlush(S s);
}
