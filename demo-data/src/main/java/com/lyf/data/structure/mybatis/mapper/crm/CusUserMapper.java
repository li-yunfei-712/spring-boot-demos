package com.lyf.data.structure.mybatis.mapper.crm;

import com.lyf.data.structure.jpa.entity.crm.CusUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/23
 * @VERSION 1.0
 * @DESC
 */
@Mapper
public interface CusUserMapper {
    @Select("SELECT * FROM cus_user")
    List<CusUserEntity> listUsers();
}
