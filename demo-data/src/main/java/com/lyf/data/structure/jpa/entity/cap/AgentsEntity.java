package com.lyf.data.structure.jpa.entity.cap;

import javax.persistence.*;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC
 */
@Entity
@Table(name = "agents", schema = "app", catalog = "")
public class AgentsEntity {
    private String aid;
    private String aname;
    private String city;
    private Integer percent;

    @Id
    @Column(name = "aid")
    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    @Basic
    @Column(name = "aname")
    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    @Basic
    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "percent")
    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AgentsEntity that = (AgentsEntity) o;

        if (aid != null ? !aid.equals(that.aid) : that.aid != null) return false;
        if (aname != null ? !aname.equals(that.aname) : that.aname != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (percent != null ? !percent.equals(that.percent) : that.percent != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = aid != null ? aid.hashCode() : 0;
        result = 31 * result + (aname != null ? aname.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (percent != null ? percent.hashCode() : 0);
        return result;
    }
}
