package com.lyf.data.structure.jpa.entity.cap;

import javax.persistence.*;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC
 */
@Entity
@Table(name = "orders", schema = "app", catalog = "")
public class OrdersEntity {
    private int ordno;
    private String month;
    private String cid;
    private String aid;
    private String pid;
    private Integer qty;
    private Double dollars;

    @Id
    @Column(name = "ordno")
    public int getOrdno() {
        return ordno;
    }

    public void setOrdno(int ordno) {
        this.ordno = ordno;
    }

    @Basic
    @Column(name = "month")
    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    @Basic
    @Column(name = "cid")
    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    @Basic
    @Column(name = "aid")
    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    @Basic
    @Column(name = "pid")
    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    @Basic
    @Column(name = "qty")
    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @Basic
    @Column(name = "dollars")
    public Double getDollars() {
        return dollars;
    }

    public void setDollars(Double dollars) {
        this.dollars = dollars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrdersEntity that = (OrdersEntity) o;

        if (ordno != that.ordno) return false;
        if (month != null ? !month.equals(that.month) : that.month != null) return false;
        if (cid != null ? !cid.equals(that.cid) : that.cid != null) return false;
        if (aid != null ? !aid.equals(that.aid) : that.aid != null) return false;
        if (pid != null ? !pid.equals(that.pid) : that.pid != null) return false;
        if (qty != null ? !qty.equals(that.qty) : that.qty != null) return false;
        if (dollars != null ? !dollars.equals(that.dollars) : that.dollars != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ordno;
        result = 31 * result + (month != null ? month.hashCode() : 0);
        result = 31 * result + (cid != null ? cid.hashCode() : 0);
        result = 31 * result + (aid != null ? aid.hashCode() : 0);
        result = 31 * result + (pid != null ? pid.hashCode() : 0);
        result = 31 * result + (qty != null ? qty.hashCode() : 0);
        result = 31 * result + (dollars != null ? dollars.hashCode() : 0);
        return result;
    }
}
