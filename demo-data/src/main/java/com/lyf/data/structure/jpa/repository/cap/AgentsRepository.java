package com.lyf.data.structure.jpa.repository.cap;

import com.lyf.data.structure.jpa.entity.cap.AgentsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC
 */
public interface AgentsRepository extends JpaRepository<AgentsEntity,Integer> {
    @Override
    <S extends AgentsEntity> S save(S s);
}
