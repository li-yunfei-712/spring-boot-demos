package com.lyf.data.connector.jdbc;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC
 */
public enum DataSourceInfoEmu {
    URL("jdbc:mysql://****:3306"),
    DRIVER_NAME("com.mysql.cj.jdbc.Driver"),
    USERNAME("***"),
    PASSWORD("***");
    private String value;

    DataSourceInfoEmu(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
}
