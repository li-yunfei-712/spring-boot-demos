package com.lyf.data.connector.jdbc;

import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC JDBC的简单连接试用
 * 一、基本步骤
 * 1. 引入相关依赖(mysql-connector-java,spring-boot-stater-jdbc)
 * 2. 配置相关信息（连接信息）
 * （1）. application.yml（全局配置文件）进行配置 JDBC数据源信息
 * （2）直接 (采用Java直接连接
 * 3. 进行连接测试（创立连接会话  Session,Statement句柄）
 *
 */
@Component
public class SimpleJdbcDemo {
    /*通常直接在此处写配置信息,即可;此次我单独采用一个枚举进行单独存储/或者其他文件;符合便于变更*/

    public Connection getCon() {
        Connection con = null;
        try{
            Class.forName(DataSourceInfoEmu.DRIVER_NAME.getValue());
            con = DriverManager.getConnection(DataSourceInfoEmu.URL.getValue(),
                    DataSourceInfoEmu.USERNAME.getValue(),DataSourceInfoEmu.PASSWORD.getValue());
        }catch (ClassNotFoundException | SQLException e){
            e.printStackTrace();
        }
        return con;
    }

    public void insert(){

    }




}
