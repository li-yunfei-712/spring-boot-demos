package com.lyf.data.domain.common;

import java.util.Date;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC
 */
public class Log {
    private Date date;
    private String level;
    private String event;

    public Log() {
    }

    public Log(Date date, String level, String event) {
        this.date = date;
        this.level = level;
        this.event = event;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    @Override
    public String toString() {
        return "Log{" +
                "date=" + date +
                ", level='" + level + '\'' +
                ", event='" + event + '\'' +
                '}';
    }
}
