package com.lyf.data.jpa;

import com.lyf.data.structure.jpa.entity.cap.AgentsEntity;
import com.lyf.data.structure.jpa.entity.slave1.TaskLog;
import com.lyf.data.structure.jpa.entity.slave1.TaskLogEntity;
import com.lyf.data.structure.jpa.repository.cap.AgentsRepository;
import com.lyf.data.structure.jpa.repository.slave1.TaskLogEntityRepository;
import com.lyf.data.structure.jpa.repository.slave1.TaskLogRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC
 */
@SpringBootTest
public class JpaTest {
    @Autowired
    AgentsRepository agentsRepository;
    @Autowired
    TaskLogRepository taskLogRepository;
    @Autowired
    TaskLogEntityRepository taskLogEntityRepository;
    @Test
    void testJpaBySingleDataSource(){
        AgentsEntity agentsEntity = new AgentsEntity();
        agentsEntity.setAid("a05");
        agentsEntity.setAname("徐占洋");
        agentsEntity.setCity("南京");
        agentsEntity.setPercent(24);
        agentsRepository.save(agentsEntity);
    }
    @Test
    void testMultipleDataSource(){
//        AgentsEntity agentsEntity = new AgentsEntity();
//        agentsEntity.setAid("a06");
//        agentsEntity.setAname("张佳其");
//        agentsEntity.setCity("南京");
//        agentsEntity.setPercent(21);
//        agentsRepository.save(agentsEntity);
//        TaskLog taskLog = new TaskLog();
//        taskLog.setDate(new Timestamp(System.currentTimeMillis()));
//        // timestamp:https://blog.csdn.net/catharryy/article/details/50766399?spm=1001.2101.3001.6650.1&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.pc_relevant_default&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.pc_relevant_default&utm_relevant_index=2
//        taskLog.setContent("this is a exception!!");
//        taskLog.setLevel("WARNING");
//        taskLogRepository.save(taskLog);
          TaskLogEntity taskLogEntity = new TaskLogEntity();
          taskLogEntity.setDate(new Timestamp(System.currentTimeMillis()));
          taskLogEntity.setContent("TEST");
          taskLogEntity.setLevel("WARNING");
          taskLogEntity.setId(2);
          //taskLogEntityRepository.save(taskLogEntity);
          taskLogEntityRepository.saveAndFlush(taskLogEntity);
          System.out.println(taskLogEntityRepository.findAll().get(0).toString());

    }
}
