package com.lyf.data.mybatis;

import com.lyf.data.structure.mybatis.mapper.cap.AgentMapper;
import com.lyf.data.structure.mybatis.mapper.crm.CusUserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/23
 * @VERSION 1.0
 * @DESC
 */
@SpringBootTest
public class TestAnnotationMapper {
    @Autowired
    AgentMapper agentMapper;
    @Autowired
    CusUserMapper cusUserMapper;
    @Test
    void test(){
        agentMapper.listAgents().forEach(u->{
            System.out.println(u);
        });
        cusUserMapper.listUsers().forEach(u->{
            System.out.println(u);
        });
    }
}
