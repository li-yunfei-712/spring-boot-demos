package com.lyf.data.jdbc;

import com.lyf.data.connector.jdbc.SimpleJdbcDemo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC
 */
@SpringBootTest
public class JdbcTest {
    @Autowired
    SimpleJdbcDemo simpleJdbcDemo;
    @Test
    void testSimpleJdbc(){
        simpleJdbcDemo.getCon();
    }
}
