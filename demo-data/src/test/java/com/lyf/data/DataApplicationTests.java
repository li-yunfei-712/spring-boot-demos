package com.lyf.data;

import com.lyf.data.domain.common.Log;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Date;

@SpringBootTest
class DataApplicationTests {
    @Autowired
    MongoTemplate mongoTemplate;
    @Test
    void contextLoads() {
       // mongoTemplate.insert(new Log(new Date(),"INFO","test.exception"));
        Query query = new Query();
       // query.addCriteria(Criteria.where(""));
        mongoTemplate.find(query,Log.class);
    }

}
