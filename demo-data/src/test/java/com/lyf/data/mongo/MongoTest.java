package com.lyf.data.mongo;

import com.lyf.data.db.mongo.MongoTemplateDemo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC
 */
@SpringBootTest
public class MongoTest {
    @Autowired
    MongoTemplateDemo mongoTemplateDemo;
    @Test
    void testMongoBaseUse(){
        mongoTemplateDemo.testBaseUse();
    }
}
