package com.lyf.data.redis;

import com.lyf.data.db.redis.RedisTemplateDemo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/19
 * @VERSION 1.0
 * @DESC
 * redis:单线程+多路IO复用（采用卖票的例子进行理解该原理）
 * -> incr,decr原子性理解
 *
 */
@SpringBootTest
public class RedisTest {
    @Autowired
    RedisTemplateDemo redisTemplateDemo;
    @Autowired
    RedisTemplate redisTemplate;
    @Test
    void testRedisBaseUse(){
        redisTemplateDemo.SimpleBaseTest();
    }

    @Test
    void StringOperate(){
        // set
        redisTemplate.opsForValue().set("strKey1","hello");
        // get
        System.out.println(redisTemplate.opsForValue().get("strKey1"));
        // mset
        HashMap<String,String> map = new HashMap<>();
        map.put("mapKey1","hello1");
        map.put("mapKey2","hello2");
        redisTemplate.opsForValue().multiSet(map);//Map.of("mapKey1","hello1")
        List<String> list = new ArrayList<>();
        redisTemplate.opsForValue().multiGet(list);
        list.stream().forEach(v->{
            System.out.println(v);
        });

        redisTemplate.opsForValue().set("num",1);
        redisTemplate.opsForValue().increment("num");//incr
        System.out.println(redisTemplate.opsForValue().get("num"));
        redisTemplate.opsForValue().decrement("num");//decr
        System.out.println(redisTemplate.opsForValue().get("num"));

        redisTemplate.expire("num", Duration.ofSeconds(30));
        System.out.println(redisTemplate.getExpire("num"));
    }

    @Test
    void ListOperate(){
        //redisTemplate.opsForList().
        redisTemplate.opsForList().leftPush("listKey1","val1");
       // redisTemplate.opsForList().
        redisTemplate.opsForList().leftPush("listKey1","val3");
        redisTemplate.opsForList().leftPush("listKey1","val1");
        redisTemplate.opsForList().remove("listKey1",2,"val1");//从左到右删除value count个

    }


    @Test
    void setOperate(){
        redisTemplate.opsForSet().add("");

        redisTemplate.convertAndSend("channel-1","hello");

    }



}
