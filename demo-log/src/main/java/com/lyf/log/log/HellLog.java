package com.lyf.log.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Controller;

/**
 * @AUTHOR LYF
 * @DATE 2022/1/20
 * @VERSION 1.0
 * @DESC
 */
@Controller
public class HellLog implements ApplicationRunner {
    Logger logger1 = LogManager.getLogger(HellLog.class);
    org.slf4j.Logger logger2 = LoggerFactory.getLogger(HellLog.class);
    java.util.logging.Logger logger3 = java.util.logging.LogManager.getLogManager().getLogger("");


    @Override
    public void run(ApplicationArguments args) throws Exception {
        logger1.info("logger1...:org.apache.logging.log4j");
        logger2.info("logger2...: org.slf4");
        logger3.info("logger3...: java.util.logging");
    }
}
